#++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#      PLOT TWO 
# plots energy and current densities along a cut that 
# traverses diagonally the zone plotted in plot one
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#----choose-labels-------------------------------------------
tar_labs = [
  ['Kinetic en. dens.','Internal en. dens.'],
  [None,'Electromagnetic en. dens.']]

#----choose-variables-to-be-plotted-----------------------
tar_varf = [
  ['Ki_'+t,'Ke_'+t,'K_'+t],
  ['Ui_'+t,'Ue_'+t,'U_'+t],
  ['enE_'+t,'enB_'+t]]
tar_kwaf = [
  ['b','darkorange','m'],
  ['b','darkorange','m'],
  ['gold','darkblue']]

tar_varc = [[J_line]]
tar_kwac = [['k:']]

#-----generate-lists-of-subplots--------------------------
my_fig = alldata.draw_canvas(tar_labs,'line')
re_fig = [my_fig[0],my_fig[1]]
er_fig = [my_fig[2]]

#-----plot-all-quantities----------------------------------
alldata.draw_cuts(my_fig,tar_varf,tar_kwaf,range_x=ranx,range_y=rany,axes_log=[False,True])
alldata.draw_lins(er_fig,tar_varc,tar_kwac,line_len=diag_len,axes_log=[False,True])

if print_me: plt.savefig(print_address+alldata.fibo_name+'_test_plot_two.png',format='png')
if show_me: plt.show()
